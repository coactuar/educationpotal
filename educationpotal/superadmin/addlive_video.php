<?php
require_once "../config.php";

    $title     = mysqli_real_escape_string($link, $_POST['vidtitle']);
    $desc     = mysqli_real_escape_string($link, $_POST['viddesc']);
    $date     = mysqli_real_escape_string($link, $_POST['viddate']);
    $url     = mysqli_real_escape_string($link, $_POST['vidurl']);
   
    $active     = mysqli_real_escape_string($link, $_POST['active']);
    $audio     = mysqli_real_escape_string($link, $_POST['audio']);
    
    $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions
    
    $img = 'poster.jpg';
    $msg = '';
    $error = 0;
    if ($_FILES['thumbnail']['error'] > UPLOAD_ERR_OK)
    {
        $query="insert into tbl_live_videos (video_title, video_desc, video_date, video_url, thumbnail_url, active,audio) values('$title', '$desc', '$date', '$url','$img', '$active','$audio')";
    //echo $query;
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        
        $vidid = mysqli_insert_id($link);
        if(isset($_POST['chklist']))
        {
        foreach($_POST['chklist'] as $val)
        {
            //$desc .= $val;
            $sql = "insert into tbl_live_batchvideos(video_id, batch) values('$vidid','$val')";
            $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
        }
        }
        
        echo "s";
    }
    else
    {
        //$img = $_FILES['thumbnail']['name'];
        $tmp = $_FILES['thumbnail']['tmp_name'];
        
        $target_dir = "../img/thumbs/";
        $thumb = $_FILES["thumbnail"]["name"];
        $thumb = str_replace(' ','',$thumb);
        $thumb = strtolower($thumb);
        $target_file = $target_dir . basename($thumb);
        $ext = strtolower(pathinfo($thumb, PATHINFO_EXTENSION));
        
        if($_FILES['thumbnail']['size'] > 200000) {
          $msg = "Image size should not be greated than 200Kb";
          $error = 1;
        }
        
        if(!in_array($ext, $valid_extensions)) 
        {
            $msg = "Only images are allowed";
            $error = 1;   
        }
        if(!$error)
        {
            $final_image = strtolower($target_file);
            $path = strtolower($final_image);
            //echo $path;
            if(move_uploaded_file($tmp,$path)) 
            {
                //success
                $query="insert into tbl_live_videos (video_title, video_desc, video_date, video_url, category, thumbnail_url, active, audio) values('$title', '$desc', '$date', '$url','$cat','$thumb', '$active','$audio')";
    echo $query;
                $res = mysqli_query($link, $query) or die(mysqli_error($link));
                $vidid = mysqli_insert_id($link);
                if(isset($_POST['chklist']))
                {
                foreach($_POST['chklist'] as $val)
                {
                    $sql = "insert into tbl_live_batchvideos(video_id, batch) values('$vidid','$val')";
                    $bv = mysqli_query($link, $sql) or die(mysqli_error($link));
                }
                }
                echo "s";    
            }
            else
            {
                echo "Image Upload failed";
            }

        }
        else
        {
            echo $msg;
        }
    }

?>