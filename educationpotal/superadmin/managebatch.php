<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	$batchid = $_GET['i'];
    if(!isset($batchid))
    {
		//header("location: batches.php");
		//exit;
	}
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Manage Batch</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="batches.php">Batches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stats.php">Statistics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>    
<div class="container-fluid">
    
    <div class="row mt-1 p-3">
<div class="col-12 text-center">
            <?php 
                $sql = "select * from tbl_batches where id='".$batchid."'";
                $res = mysqli_query($link, $sql) or die(mysqli_error($link));
                //echo $sql;
                $data = mysqli_fetch_assoc($res);
                $batchname = $data['batch_name'];
            ?>
            <form id="managebatch-form" method="post">
            <h1>Manage <?php echo $batchname; ?></h1>
              <div id="message"></div>
              <ul class="list-unstyled">
              <?php 
                $sql = "select tbl_videos.id, video_title, tbl_categories.category from tbl_videos, tbl_categories where tbl_videos.category=tbl_categories.id";
                $res = mysqli_query($link, $sql) or die(mysqli_error($link));
                
                while($data = mysqli_fetch_assoc($res)){
                    $s = "select * from tbl_batchvideos where video_id ='".$data['id']."' and batch='".$batchname."'";  
                    //echo $s.'<br>';
                    $r = mysqli_query($link, $s) or die(mysqli_error($link));  
                
                 ?>
                 <li class="text-left">
                    <input type="checkbox" <?php 
                        if(mysqli_affected_rows($link) > 0)
                        {
                            echo 'checked';
                        }
                        
                        ?> class="form-check-inline" value="<?php echo $data['id']; ?>" onClick="updBatchVid('<?php echo $data['id']; ?>')" name="chklist[]"/><?php echo $data['video_title'].' ['.$data['category'].']'; ?>
                 </li>
                 <?php
                }
                
              ?>
              </ul>
              
              
        <!--      
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <input type="text" class="form-control" placeholder="Video Title" aria-label="Video Title" aria-describedby="basic-addon1" name="vidtitle" id="vidtitle" value="<?php echo $data['video_title']; ?>" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <select id="category" name="category" class="form-control">
                           <option value="0">Select Category</option>
                      <?php
                          $csql = "select * from tbl_categories";
                          $cres = mysqli_query($link, $csql) or die(mysqli_error($link));
                          while($cdata = mysqli_fetch_assoc($cres))
                          {
                      ?>
                           <option value="<?php echo $cdata['id']; ?>" <?php if($data['category'] == $cdata['id']) echo 'selected'; ?>><?php echo $cdata['category']; ?></option>
                      <?php
                          }
                      ?>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Video Description" aria-label="Video Desc." aria-describedby="basic-addon1" name="viddesc" id="viddesc" rows="4"><?php echo $data['video_desc']; ?></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Video URL" aria-label="Video URL" aria-describedby="basic-addon1" name="vidurl" id="vidurl" rows="4" required><?php echo $data['video_url']; ?></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <input type="date" class="form-control" placeholder="Video Date" aria-label="Video Date" aria-describedby="basic-addon1" name="viddate" id="viddate" value="<?php echo $date; ?>">
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="audio" name="audio" class="form-control" required>
                           <option value="1" <?php if($data['audio'] == '1') echo 'selected'; ?>>Only Audio</option>
                           <option value="0"<?php if($data['audio'] == '0') echo 'selected'; ?>>Video+Audio</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="active" name="active" class="form-control" required>
                           <option value="1" <?php if($data['active'] == '1') echo 'selected'; ?>>Active</option>
                           <option value="0" <?php if($data['active'] == '0') echo 'selected'; ?>>Not Active</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <div class="row">
                          <div class="col-6">
                              <img src="../img/thumbs/<?php echo $data['thumbnail_url']; ?>" id="thumbPreview" class="img-fluid thumbnail" alt=""/> 
                          </div>
                          <div class="col-6">
                              <div class="upload-btn-wrapper">
                                <button class="btn-upload">Upload thumbnail</button>
                                <input type="file" name="thumbnail" id="thumbnail" onChange="displayImage(this)" />
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-left">
                Select Batches:<br><br>
                <?php
                    $s = "select * from tbl_batches where active='1' order by batch_name asc";
                    $r = mysqli_query($link, $s) or die(mysqli_error($link));
                    while($d = mysqli_fetch_assoc($r)){
                     $b_name = $d['batch_name'];
                     ?>
                     <input type="checkbox" <?php 
                        $sql = "select * from tbl_batchvideos where video_id ='$vid' and batch='".$b_name."'";  
                        $res = mysqli_query($link, $sql) or die(mysqli_error($link));  
                        if(mysqli_affected_rows($link) > 0)
                        {
                            echo 'checked';
                        }
                        
                        ?> class="form-check-inline" value="<?php echo $b_name; ?>" name="chklist[]"/><?php echo $b_name; ?> <br>
                     <?php   
                    }
                ?>
                
                
                </div>
             </div>
             <div class="row mt-2 mb-2">    
                <div class="col-12 col-md-4 offset-md-2">
                    <div class="input-group mt-1 mb-1">
                      <input type="hidden" name="vidthumb" id="vidthumb" value="<?php echo $data['thumbnail_url']; ?>">
                      <input type="hidden" name="vidid" id="vidid" value="<?php echo $_GET['v']; ?>">
                      <button class="mt-1 btn btn-block" type="submit">Update</button>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="input-group mt-1 mb-1">
                      <a class="btn btn-block btn-danger" href="audiovideo.php">Cancel</a>
                    </div>
                </div>
              </div>
             -->
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){


});

function updBatchVid(vidid)
{
    $.ajax({
        url: 'ajax.php',
         data: {action: 'updbatchvid', vidid: vidid, batch: '<?php echo $batchid; ?>'},
         type: 'post',
         success: function(output) {
             console.log(output);
                      }
   });
}


</script>
</body>
</html>