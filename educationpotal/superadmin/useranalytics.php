<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Viewers Analytics</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="batches.php">Batches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stats.php">Statistics</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="livevidanalytics.php">Live Session</a>
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="recordlive.php">Live Sessions Record</a>
      </li>
    </ul>    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 p-3">
        <div class="col-12">
            <div id="video-report">
            <table class="table table-striped table-dark">
            <thead class="thead-dark">
            <tr>
                <th align="center">Batch 53</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 53'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 53') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 54</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 54'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 54') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 55</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 55'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 55') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 56</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 56'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 56') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 57</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 57'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 57') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 58</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 58'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 58') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th align="center">Batch 59</th><th colspan="2">Total Audio/Videos Accessible: 
                <?php
                $sql = "SELECT count(*) as cnt FROM tbl_batchvideos where batch='Batch 59'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                $data = mysqli_fetch_assoc($rs_result);
                
                echo $data['cnt'];
                    
                ?>
                </th>
            </tr></thead>    
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th width="200">Mobile No.</th>
                <th width="250">Total Watched</th>
            </tr></thead>
            <?php
                $sql = "select count(DISTINCT video_id) as cnt, user_id from tbl_viewers where user_id in (select DISTINCT user_id from tbl_viewers where user_id in (SELECT id FROM `tbl_users` where batch='Batch 59') ORDER BY user_id) GROUP by user_id order by cnt desc LIMIT 10";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                
                while($data = mysqli_fetch_assoc($rs_result))
                {
                    $q = "select * from tbl_users where id ='".$data['user_id']."'";
                    $r = mysqli_query($link, $q);
                    $d = mysqli_fetch_assoc($r);
            ?>
            <tr>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo '+'.$d['cntry_code'].'-'.$d['mobile_num']; ?></td>
                <td><?php echo $data['cnt']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            
            </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
</script>

</body>
</html>