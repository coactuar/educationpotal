<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Video Analytics</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="batches.php">Batches</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stats.php">Statistics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 p-3">
        <div class="col-12">
            <div id="video-report">
            <?php
                $vid = $_GET['vid'];
                $sql = "SELECT * FROM tbl_videos where active='1' and id ='$vid'";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                if(mysqli_affected_rows($link) > 0)
                {
                    $data = mysqli_fetch_assoc($rs_result);
                }
                else
                {
                    header('location: vidanalytics.php');
                    exit;
                }
            ?>
            <b>Video Title:</b> <?php echo $data['video_title']; ?>
            <table class="table table-striped table-dark">
            <thead class="thead-dark"><tr>
                <th>Name</th>
                <th>Mobile No.</th>
                <th>Batch</th>
                <th>No. of Views</th>
            </tr></thead>
            <?php
                $sql = "SELECT count(*) as views, user_id, name, cntry_code, mobile_num, batch FROM tbl_viewers, tbl_users where video_id ='$vid' and tbl_viewers.user_id = tbl_users.id GROUP by tbl_viewers.user_id order by views desc";  
                $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                //echo $sql;
                while($data = mysqli_fetch_assoc($rs_result))
                {
            ?>
            <tr>
                <td><?php echo $data['name']; ?></td>
                <td><?php echo '+'.$data['cntry_code'].'-'.$data['mobile_num']; ?></td>
                <td><?php echo $data['batch']; ?></td>
                <td><?php echo $data['views']; ?></td>
            </tr>
            <?php        
                    
                }
            ?>
            </table>
            </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
</script>

</body>
</html>