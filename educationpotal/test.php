<?php
// page1.php

//session_start();
require_once "config.php";

echo 'Welcome to test page';

$_SESSION['favcolor'] = 'green';
$_SESSION['animal']   = 'human
';
$_SESSION['time']     = time();

// Works if session cookie was accepted
//echo '<br /><a href="page2.php">page 2</a>';

// Or maybe pass along the session id, if needed
//echo '<br /><a href="page2.php?' . SID . '">page 2</a>';

echo '<br>The ' . $_SESSION['animal'] . ' is ' . $_SESSION['favcolor'];
?>
