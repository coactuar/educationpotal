<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Add Document</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>  
<div class="container-fluid">
    
    <div class="row p-3">
<div class="col-12 text-center">
            <form id="adddoc-form" method="post">
            <h1>Add Document</h1>
              <div id="message"></div>
              <div class="row">
                <div class="col-12 col-md-12">
                    <div class="input-group mt-1 mb-1">
                      <input type="text" class="form-control" placeholder="Title" aria-label="Title" aria-describedby="basic-addon1" name="doctitle" id="doctitle" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Description" aria-label="Desc." aria-describedby="basic-addon1" name="docdesc" id="docdesc" rows="4"></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="URL" aria-label="URL" aria-describedby="basic-addon1" name="docurl" id="docurl" rows="4" required></textarea>
                    </div>
                </div>
                
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="active" name="active" class="form-control" required>
                           <option value="1">Active</option>
                           <option value="0">Not Active</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-left">
                Select Batches:<br><br>
                <?php
                    $s = "select * from tbl_batches where active='1' order by batch_name asc";
                    $r = mysqli_query($link, $s) or die(mysqli_error($link));
                    while($d = mysqli_fetch_assoc($r)){
                     $b_name = $d['batch_name'];
                     ?>
                     <input type="checkbox" class="form-check-inline" value="<?php echo $b_name; ?>" name="chklist[]"/><?php echo $b_name; ?> <br>
                     <?php
                    }
                
                ?>
                </div>
            </div>
            <div class="row mt-2 mb-2">    
                <div class="col-12 col-md-4 offset-md-2">
                    <div class="input-group mt-1 mb-1">
                      <button class="btn btn-block" type="submit">Add Dcoument</button>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="input-group mt-1 mb-1">
                      <a class="btn btn-block btn-danger" href="documents.php">Cancel</a>
                    </div>
                </div>
              </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){

  $(document).on('submit', '#adddoc-form', function()
    {  
            $.post('add_doc.php', $(this).serialize(), function(data)
            {
                if(data=="s")
                {
                  window.location.href='documents.php';
                }
                else 
                {
                  $('#message').text(data);
                  $('#message').addClass('alert alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });

});



</script>

</body>
</html>