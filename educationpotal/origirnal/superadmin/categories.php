<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Categories</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
     
    <div class="row mt-1">
        <div class="col-12">
            <form class="form-inline" id="add-cat">
              <div class="row">
                <div class="col-12 col-md-6">
                    <input type="text" class="form-control" id="category" name="category">
                </div>
                <div class="col-12 col-md-6">
                    <button type="submit" class="btn btn-default">Add Category</button>
                </div>
              </div>
              
            </form>
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-12">
            <div id="categories"></div>
        </div>
    </div>
</div>


<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
    getCategories('1');
});

function update(pageNum)
{
  getCategories(pageNum);
}

function getCategories(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getcategories', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#categories").html(response);
            
        }
    });
    
}
$(function(){
     
	$(document).on('submit', '#add-cat', function()
    {  
            $.post('add_cat.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                 getCategories('1'); 
                }
                else 
                {
                  $('#message').text(data);
                  $('#message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});

</script>

</body>
</html>