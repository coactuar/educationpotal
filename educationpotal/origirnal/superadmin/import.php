<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Import Users</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-5 p-0">
        <div class="col-12 col-md-8 offset-md-2">
            <form action="#" method="post" name="import-form" id="import-form" enctype="multipart/form-data">
                <div>
                    <label>Choose CSV
                        File</label> <input type="file" name="file"
                        id="file" accept=".csv" required>
                    <button type="submit" id="submit" name="import"
                        class="btn-submit">Import Users</button>
            
                </div>
            
            </form>
            <div id="response"></div>
            
        </div>
    </div>
    
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
	$(document).on('submit', '#import-form', function()
    {  
    $('#response').text('Updating database...').addClass('alert-info').fadeIn()
      var form = $(this);
      var formdata = false;
      if (window.FormData){
          formdata = new FormData(form[0]);
      }
  
      var formAction = form.attr('action');
      $.ajax({
          url         : 'importusers.php',
          data        : formdata ? formdata : form.serialize(),
          cache       : false,
          contentType : false,
          processData : false,
          type        : 'POST',
          success     : function(data, textStatus, jqXHR){
              // Callback code
              $('#response').text(data).addClass('alert-info').fadeIn();
          }
      });
            
    
      
      return false;
    });
});</script>

</body>
</html>