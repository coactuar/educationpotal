<?php
require_once "config.php";

    $batch     = mysqli_real_escape_string($link, $_POST['batch']);
    $phone     = mysqli_real_escape_string($link, $_POST['phnNum']);
    $code     = mysqli_real_escape_string($link, $_POST['country']);
    
    $query="select * from tbl_users where mobile_num='$phone' and cntry_code='$code' and batch='$batch'";
    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
    //echo $query;
    if (mysqli_affected_rows($link) > 0) 
    {
        //$row = mysqli_fetch_row($res); 
        $row = mysqli_fetch_assoc($res);
        
        if($row['active'] == '0')
        {
            echo '-2';
        }
        else
        {
            $name = $row["name"];
            $id = $row["id"];
            try{
                $today=date("Y/m/d H:i:s");
        
                $dateTimestamp1 = strtotime($row['logout_date']);
                $dateTimestamp2 = strtotime($today);
                //echo $row[8];
                if ($dateTimestamp1 > $dateTimestamp2)
                {
                  echo "-1";
                }
                else
                {
                  $login_date   = date('Y/m/d H:i:s');
                  $logout_date   = date('Y/m/d H:i:s', time() + 30);
    
                  $query="UPDATE tbl_users set login_date='$login_date', logout_date='$logout_date', logout_status='1'  where mobile_num='$phone' and cntry_code='$code' and batch='$batch'";
                  $res = mysqli_query($link, $query) or die(mysqli_error($link));
                  
                  $_SESSION["user_phone"]    = $phone;
                  $_SESSION["user_id"]     = $id;
                  $_SESSION["user_code"]     = $code;
                  $_SESSION["user_name"]     = $name;
                  $_SESSION["user_batch"]     = $batch;
    
                  /*if(isset($_POST['remember'])) { 
                      $_SESSION["user_remember"] = '1';
                  } 
                  else
                  {
                      //echo "Don't remember";
                      if(isset($_SESSION["user_remember"]))
                      {
                          unset($_SESSION["user_remember"]);
                      }
                  }
                  */
                  //echo $_SESSION['user_id'];
                  if( (isset($_SESSION["user_phone"])) && (isset($_SESSION["user_name"])))
                  {
                    echo "s";
                  }
                  else
                  {
                      session_destroy();
                      echo "We are not able to log you in right now. Please try again laters or try using another browser.";
                  }
                }
                        
       
                
            }
            catch(PDOException $e){
              echo $e->getMessage();
            }
                
        }
        
    }
    else{
        echo '0';
    }

?>