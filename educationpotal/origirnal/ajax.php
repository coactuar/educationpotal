<?php
require_once "config.php";
require_once "functions.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        case 'update' : 
		
        $phone=$_SESSION["user_phone"];
        $code=$_SESSION["user_code"];
        $batch=$_SESSION["user_batch"];
		
		$query="select logout_status,login_date from tbl_users where mobile_num='$phone' and cntry_code='$code' and batch='$batch'";
        $res = mysqli_query($link, $query) or die(mysqli_error($link));
        if(mysqli_affected_rows($link) <=0 )
        {
            echo "0";
            exit;
        }
        
        $data = mysqli_fetch_assoc($res);

        $logout=$data['logout_status'];
        $login_time = $data['login_date'];

        if($logout=="1")
        {

          $logout_date  = date('Y/m/d H:i:s', time() + 30);
          
          $query="UPDATE tbl_users set logout_date='$logout_date' where mobile_num='$phone' and cntry_code='$code' and batch='$batch'";
          $res = mysqli_query($link, $query) or die(mysqli_error($link));
          
        }
        
        else{
            
          $logout_date   = date('Y/m/d H:i:s');
          $query="UPDATE tbl_users set logout_date='$logout_date' where mobile_num='$phone' and cntry_code='$code' and batch='$batch'";
          $res = mysqli_query($link, $query) or die(mysqli_error($link));
          
          echo "0";
        }

		break;
        
        case 'getvids':
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            $audio = '0'; //$_POST['audio'];
            $start_from = ($page-1) * $limit;
            //$sql = "SELECT COUNT(id) as count FROM tbl_videos where active='1'";  
            $sql = "SELECT COUNT(*) as count FROM tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1'  and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.audio='$audio'"; 
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            //echo $sql; //$total_records;
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row mt-3">
            <?php		
              $key = $_POST['keyword'];
              //$query="select * from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1'";
              $query = "select tbl_videos.id, video_title, video_desc, video_date, thumbnail_url, views, active from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1' and tbl_videos.audio='$audio'";
            
              if($key != '')
              {
                 $query .= " and  (video_title like '%".$key."%'";
                 $query .= " or  video_desc like '%".$key."%')";
              }
              $query .= " LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              //echo $query;
              if(mysqli_affected_rows($link) > 0)
              {
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                 <div class="col-12 col-md-6 col-lg-4">
                  <a href="watch.php?v=<?php echo $data['id']; ?>"><img src="img/thumbs/<?php echo $data['thumbnail_url']; ?>" class="img-fluid"  alt=""/> </a>
                  <div class="meta">
                  <h3><a href="watch.php?v=<?php echo $data['id']; ?>"><?php echo $data['video_title']; ?></a></h3>
                  <h4><?php echo $data['video_desc']; ?></h4>
                  <h5><?php 
                  echo $data['views']; 
                  //echo $view;
                  ?> Views</h5> 
                  <?php if($data['video_date'] != '')
                  {
                        $date=date_create($data['video_date']);
                        echo ' | <h5>'.date_format($date,"M d, Y").'</h5>';
                  } ?>
                  </div>
                 </div> 
            <?php			
                }
              }
              else
              {
                  echo 'No videos found.';
              }
            ?>
            </div>   
            <?php
            if($total_pages > 1)
            {
            ?>
            <nav>
              <ul class="pagination pagination-sm justify-content-end">
                <?php 
                //echo $total_pages;
                if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
                
              </ul>
            </nav>
            <?php
            }
        break;
        
        case 'getcatvids':
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            $cat = $_POST['catid'];
            $start_from = ($page-1) * $limit;
//            $sql = "SELECT COUNT(id) as count FROM tbl_videos where active='1' and category='$cat'"; 
            $sql = "SELECT COUNT(*) as count FROM tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1' and category='$cat' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.audio='0'";  
            //echo $sql;
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row mt-3">
            <?php		
              $key = $_POST['keyword'];
//              $query="select * from tbl_videos where active='1' and category='$cat'";
              $query = "select tbl_videos.id, video_title, video_desc, video_date, thumbnail_url, views, active from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1'  and category='$cat'  and tbl_videos.audio='0'";
              if($key != '')
              {
                 $query .= " and  (video_title like '%".$key."%'";
                 $query .= " or  video_desc like '%".$key."%')";
              }
              $query .= " LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              //echo $query;
              if(mysqli_affected_rows($link) > 0)
              {
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                 <div class="col-12 col-md-6 col-lg-4">
                  <a href="watch.php?v=<?php echo $data['id']; ?>"><img src="img/thumbs/<?php echo $data['thumbnail_url']; ?>" class="img-fluid"  alt=""/> </a>
                  <div class="meta">
                  <h3><a href="watch.php?v=<?php echo $data['id']; ?>"><?php echo $data['video_title']; ?></a></h3>
                  <h4><?php echo $data['video_desc']; ?></h4>
                  <h5><?php 
                  echo $data['views']; 
                  //echo $view;
                  ?> Views</h5> 
                  <?php if($data['video_date'] != '')
                  {
                        $date=date_create($data['video_date']);
                        echo ' | <h5>'.date_format($date,"M d, Y").'</h5>';
                  } ?>
                  </div>
                 </div> 
              <?php			
                }
              }
              else
              {
                  echo 'No videos found.';
              }
            ?>
            </div>   
            <?php
            if($total_pages > 1)
            {
            ?>
            <nav>
              <ul class="pagination pagination-sm justify-content-end">
                <?php 
                //echo $total_pages;
                if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
                
              </ul>
            </nav>
            <?php
            }
        break;
        
        case 'updateview': 
            $end_date   = date('Y/m/d H:i:s', time() + 5);
            $query="UPDATE tbl_viewers set end_time='$end_date' where id='".$_POST['view']."'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
		break;
        
        
        case 'getcatauds':
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            $limit = 15;
            $audio = $_POST['audio'];
            $cat = $_POST['catid'];
            $start_from = ($page-1) * $limit;
            //$sql = "SELECT COUNT(id) as count FROM tbl_videos where active='1'";  
            $sql = "SELECT COUNT(*) as count FROM tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1'  and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.audio='$audio' and category='$cat'"; 
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            //echo $sql; //$total_records;
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row mt-3">
            <?php		
              $key = $_POST['keyword'];
              //$query="select * from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1'";
              $query = "select tbl_videos.id, video_title, video_desc, video_date, thumbnail_url, views, active from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_batchvideos.video_id=tbl_videos.id and tbl_videos.active='1' and tbl_videos.audio='$audio' and category='$cat' order by video_title";
            
              if($key != '')
              {
                 $query .= " and  (video_title like '%".$key."%'";
                 $query .= " or  video_desc like '%".$key."%')";
              }
              //$query .= " LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              //echo $query;
              if(mysqli_affected_rows($link) > 0)
              {
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                 <div class="col-12 col-md-6 col-lg-4">
                    <div class="row">
                        <div class="col-4 col-md-1">
                            <a href="listen.php?v=<?php echo $data['id']; ?>"><i class="fa fa-headphones fa-2x"></i></a>
                        </div>
                        <div class="col-8 col-md-10">
                            <div class="meta audio">
                                <h3><a href="listen.php?v=<?php echo $data['id']; ?>"><?php echo $data['video_title']; ?></a></h3>
                                <h4><?php echo $data['video_desc']; ?></h4>
                                <h5><?php 
                                echo $data['views']; 
                                //echo $view;
                                ?> Views</h5> 
                                <?php if($data['video_date'] != '')
                                {
                                      $date=date_create($data['video_date']);
                                      echo ' | <h5>'.date_format($date,"M d, Y").'</h5>';
                                } ?>
                            </div>
                        </div>
                    </div>
                  
                  
                 </div> 
            <?php			
                }
              }
              else
              {
                  echo 'No audio found.';
              }
            ?>
            </div>   
            <?php
            if($total_pages > 1)
            {
            ?>
            <!--<nav>
              <ul class="pagination pagination-sm justify-content-end">
                <?php 
                //echo $total_pages;
                if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
                
              </ul>
            </nav>-->
            <?php
            }
        break;

        case 'getdocs':
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            $limit = 15;
            $start_from = ($page-1) * $limit;
            //$sql = "SELECT COUNT(id) as count FROM tbl_videos where active='1'";  
            $sql = "SELECT COUNT(*) as count FROM tbl_batchdocs, tbl_documents where tbl_batchdocs.batch='".$_SESSION['user_batch']."' and tbl_documents.active='1'  and tbl_batchdocs.doc_id=tbl_documents.id"; 
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            //echo $sql; //$total_records;
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row mt-3">
            <?php		
              $key = $_POST['keyword'];
              //$query="select * from tbl_batchvideos, tbl_videos where tbl_batchvideos.batch='".$_SESSION['user_batch']."' and tbl_videos.active='1'";
              $query = "select tbl_documents.id, doc_title, doc_desc, views, active from tbl_batchdocs, tbl_documents where tbl_batchdocs.batch='".$_SESSION['user_batch']."' and tbl_batchdocs.doc_id=tbl_documents.id and tbl_documents.active='1' order by id";
            
              if($key != '')
              {
                 $query .= " and  (doc_title like '%".$key."%'";
                 $query .= " or  doc_desc like '%".$key."%')";
              }
              $query .= " LIMIT $start_from, $limit";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              //echo $query;
              if(mysqli_affected_rows($link) > 0)
              {
                while($data = mysqli_fetch_assoc($res))
                {
                ?>
                 <div class="col-12 col-md-6 col-lg-4">
                    <div class="row">
                        <div class="col-4 col-md-1">
                            <a href="view.php?d=<?php echo $data['id']; ?>"><i class="far fa-file-pdf fa-2x"></i></a>
                        </div>
                        <div class="col-8 col-md-10">
                            <div class="meta audio">
                                <h3><a href="view.php?d=<?php echo $data['id']; ?>"><?php echo $data['doc_title']; ?></a></h3>
                                <h4><?php echo $data['doc_desc']; ?></h4>
                                <h5><?php 
                                echo $data['views']; 
                                //echo $view;
                                ?> Views</h5> 
                             </div>
                        </div>
                    </div>
                  
                  
                 </div> 
            <?php			
                }
              }
              else
              {
                  echo 'No audio found.';
              }
            ?>
            </div>   
            <?php
            if($total_pages > 1)
            {
            ?>
            <nav>
              <ul class="pagination pagination-sm justify-content-end">
                <?php 
                //echo $total_pages;
                if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update('<?php echo $i;?>','<?php echo $key;?>')" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
                
              </ul>
            </nav>
            <?php
            }
        break;


        
    }
    
}


?>