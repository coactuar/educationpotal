<?php
require_once "../config.php";

$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

$message = 'Error updating users';
    if(in_array($_FILES["file"]["type"],$allowedFileType)){
      
      $targetPath = 'uploads/'.$_FILES['file']['name'];
      move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
      $file = $targetPath;
      $handle = fopen($file, "r");
      $c = 0;
      $n = 0;
      $d = 0;
      while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
      {                
          $batch = $filesop[0];
          $name = $filesop[1];
          $code = $filesop[2];
          $phone = $filesop[3];
          
          if(($batch =='') || ($name=='') || ($code =='') || ($phone==''))
          {
              //do nothing
          }
          else
          {
            $query="select * from tbl_users where batch='$batch' and mobile_num='$phone' and cntry_code='$code'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
            if (mysqli_affected_rows($link) > 0) 
            {
                $d = $d + 1;
            }
            else{             
              $query="insert into tbl_users (batch, name, mobile_num, cntry_code, logout_status) values('$batch','$name', '$phone', '$code','0')";
              $res = mysqli_query($link, $query) or die(mysqli_error($link));
              $n = $n+1;
            }
          }
        
          $c = $c + 1;
          
      }
      $message = $n . " users imported in database. ";
      if($d > 0)
      {
          $message .= $d . " duplicate records found.";
      }
    
    }
    else
    { 
        $message = "Invalid File Type. Upload Excel File.";
    }

echo $message;
?>