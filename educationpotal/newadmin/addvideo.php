<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Video</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>  
<div class="container-fluid">
    
    <div class="row p-3">
<div class="col-12 text-center">
            <form id="addvideo-form" method="post" enctype="multipart/form-data">
            <h1>Add Video</h1>
              <div id="login-message"></div>
              <div class="row">
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <input type="text" class="form-control" placeholder="Video Title" aria-label="Video Title" aria-describedby="basic-addon1" name="vidtitle" id="vidtitle" required>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <select id="category" name="category" class="form-control">
                           <option value="0">Select Category</option>
                      <?php
                          $sql = "select * from tbl_categories";
                          $res = mysqli_query($link, $sql) or die(mysqli_error($link));
                          //echo $query;
                          while($data = mysqli_fetch_assoc($res))
                          {
                      ?>
                           <option value="<?php echo $data['id']; ?>"><?php echo $data['category']; ?></option>
                      <?php
                          }
                      ?>
                      </select>
                    </div>
                </div>
                
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Video Description" aria-label="Video Desc." aria-describedby="basic-addon1" name="viddesc" id="viddesc" rows="4"></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Video URL" aria-label="Video URL" aria-describedby="basic-addon1" name="vidurl" id="vidurl" rows="4" required></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <input type="date" class="form-control" placeholder="Video Date" aria-label="Video Date" aria-describedby="basic-addon1" name="viddate" id="viddate">
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="audio" name="audio" class="form-control" required>
                           <option value="1">Only Audio</option>
                           <option value="0" selected>Video+Audio</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="active" name="active" class="form-control" required>
                           <option value="1">Active</option>
                           <option value="0">Not Active</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <div class="row">
                          <div class="col-6">
                              <img src="../img/thumbs/poster.jpg" id="thumbPreview" class="img-fluid thumbnail" alt=""/> 
                          </div>
                          <div class="col-6">
                              <div class="upload-btn-wrapper">
                                <button class="btn-upload">Upload thumbnail</button>
                                <input type="file" name="thumbnail" id="thumbnail" onChange="displayImage(this)" />
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-left">
                Select Batches:<br><br>
                <?php
                    $s = "select * from tbl_batches where active='1' order by batch_name asc";
                    $r = mysqli_query($link, $s) or die(mysqli_error($link));
                    while($d = mysqli_fetch_assoc($r)){
                     $b_name = $d['batch_name'];
                     ?>
                     <input type="checkbox" class="form-check-inline" value="<?php echo $b_name; ?>" name="chklist[]"/><?php echo $b_name; ?> <br>
                     <?php
                    }
                
                ?>
                <!--<input type="checkbox" class="form-check-inline" value="Batch 53" name="chklist[]"/>Batch 53 <br>
                
                <input type="checkbox" class="form-check-inline" value="Batch 54" name="chklist[]"/>Batch 54 <br>
                
                <input type="checkbox" class="form-check-inline" value="Batch 55" name="chklist[]"/>Batch 55 <br>
                
                <input type="checkbox" class="form-check-inline" value="Batch 56" name="chklist[]"/>Batch 56 <br>
                
                <input type="checkbox" class="form-check-inline" value="Batch 57" name="chklist[]"/>Batch 57 <br>

                <input type="checkbox" class="form-check-inline" value="Batch 58" name="chklist[]"/>Batch 58 <br>

                <input type="checkbox" class="form-check-inline" value="Batch 59" name="chklist[]"/>Batch 59 <br>-->
                </div>
            </div>
            <div class="row mt-2 mb-2">    
                <div class="col-12 col-md-4 offset-md-2">
                    <div class="input-group mt-1 mb-1">
                      <button class="btn btn-block" type="submit">Add Video</button>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="input-group mt-1 mb-1">
                      <a class="btn btn-block btn-danger" href="audiovideo.php">Cancel</a>
                    </div>
                </div>
              </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#addvideo-form', function()
  {
      var form = $(this);
      var formdata = false;
      if (window.FormData){
          formdata = new FormData(form[0]);
      }
  
      var formAction = form.attr('action');
      $.ajax({
          url         : 'add_video.php',
          data        : formdata ? formdata : form.serialize(),
          cache       : false,
          contentType : false,
          processData : false,
          type        : 'POST',
          success     : function(data, textStatus, jqXHR){
              // Callback code
              if(data =='s')
              {
                window.location = 'audiovideo.php';   
              }
          }
      });
      
      /*$.post('add_video.php', $(this).serialize(), function(data)
      {
          if(data =='s')
          {
            window.location = 'videos.php';   
          }
        
      });*/
  
       return false;
  });

});


function triggerClick(e) {
  document.querySelector('#profileImage').click();
}
function displayImage(e) {
  if (e.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#thumbPreview').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
</script>

</body>
</html>