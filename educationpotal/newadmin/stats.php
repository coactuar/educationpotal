<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Stats</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      <li class="nav-item ">
        <a class="nav-link" href="users.php">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="categories.php">Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="vidanalytics.php">Video Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="useranalytics.php">Viewers Analytics</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stats.php">Statistics</a>
      </li>
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
     
    </div><div class="row p-3">
        <div class="col-12 col-md-6 offset-md-3">
            <h5>Users</h5>
            <?php
            $total = 0;
            $sql = "SELECT count(*) as count FROM `tbl_users`";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            echo "<b>Registerd Users:</b> " . $data['count']."<br>";
            $sql = "SELECT count(*) as count FROM `tbl_users` where login_date != 'null'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            echo "<b>Active Users:</b> " . $data['count']."<br>";
            ?>
            <br>
            <h5>Content</h5>
            <?php
            $total = 0;
            $sql = "SELECT count(*) as count FROM `tbl_videos` where audio='0'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            echo "<b>Videos:</b> " . $data['count']."<br>";
            $total += $data['count'];
            $sql = "SELECT count(*) as count FROM `tbl_videos` where audio='1'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            echo "<b>Audios:</b> " . $data['count']."<br>";
            $total += $data['count'];
            $sql = "SELECT count(*) as count FROM `tbl_documents`";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            echo "<b>Documents:</b> " . $data['count']."<br>";
            $total += $data['count'];
            echo "<br><b>Total:</b> " . $total."<br>";
            ?>
            <br>
            <h5>Total Watch Time </h5>
            <?php
            $today=date("Y/m/d H:i:s");
            
            $date=date_create($today);
            $month = date_format($date,"m");
            echo "<b>".date_format($date,"M Y").":</b> ";
            
            
            $sql = "SELECT SUM(TIMESTAMPDIFF(SECOND, start_time, end_time)) as totaltime FROM `tbl_viewers` where month(start_time)='$month' and month(end_time)='$month'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            
            $totaltime = $data['totaltime'];
            
            $time = secToHR($totaltime);
            echo $time . "<br>";
            
            echo $prevmonth = "<b>".date("M Y", strtotime("-1 months")).":</b> ";
            $month = date_format($date,"m") - 1;
            
            $sql = "SELECT SUM(TIMESTAMPDIFF(SECOND, start_time, end_time)) as totaltime FROM `tbl_viewers` where month(start_time)='$month' and month(end_time)='$month'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            
            $totaltime = $data['totaltime'];
            
            $time = secToHR($totaltime);
            echo $time . "<br>";
            
            echo $prevmonth = "<b>".date("M Y", strtotime("-2 months")).":</b> ";
            $month = date_format($date,"m") - 2;
            
            $sql = "SELECT SUM(TIMESTAMPDIFF(SECOND, start_time, end_time)) as totaltime FROM `tbl_viewers` where month(start_time)='$month' and month(end_time)='$month'";
            $res = mysqli_query($link, $sql);
            $data = mysqli_fetch_assoc($res);
            
            $totaltime = $data['totaltime'];
            
            $time = secToHR($totaltime);
            echo $time . "<br>";
            
            ?>
        </div>
    </div>
</div>

<?php
function secToHR($seconds) {
  //$days = floor($seconds /   
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return $hours > 0 ? "$hours hours, $minutes minutes" : ($minutes > 0 ? "$minutes minutes, $seconds seconds" : "$seconds seconds");//
}
?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
</script>

</body>
</html>