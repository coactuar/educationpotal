<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	$did = $_GET['d'];
    if(!isset($did))
    {
		header("location: documents.php");
		exit;
	}
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> Edit Document</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
     
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>    
<div class="container-fluid">
    
    <div class="row mt-1 p-3">
<div class="col-12 text-center">
            <?php 
                $sql = "select * from tbl_documents where id='$did'";
                $res = mysqli_query($link, $sql) or die(mysqli_error($link));
                //echo $sql;
                $data = mysqli_fetch_assoc($res);
            ?>
            <form id="editdoc-form" method="post">
            <h1>Edit Document</h1>
              <div id="message"></div>
              <div class="row">
                <div class="col-12 col-md-12">
                    <div class="input-group mt-1 mb-1">
                      <input type="text" class="form-control" placeholder="Title" aria-label="Title" aria-describedby="basic-addon1" name="doctitle" id="doctitle" value="<?php echo $data['doc_title']; ?>" required>
                    </div>
                </div>
                
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="Description" aria-label="Desc." aria-describedby="basic-addon1" name="docdesc" id="docdesc" rows="4"><?php echo $data['doc_desc']; ?></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="input-group mt-1 mb-1">
                      <textarea class="form-control" placeholder="URL" aria-label="URL" aria-describedby="basic-addon1" name="docurl" id="docurl" rows="4" required><?php echo $data['doc_url']; ?></textarea>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <div class="input-group mt-1 mb-1">
                      <select id="active" name="active" class="form-control" required>
                           <option value="1" <?php if($data['active'] == '1') echo 'selected'; ?>>Active</option>
                           <option value="0" <?php if($data['active'] == '0') echo 'selected'; ?>>Not Active</option>
                      </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 text-left">
                Select Batches:<br><br>
                <?php
                    $s = "select * from tbl_batches where active='1' order by batch_name asc";
                    $r = mysqli_query($link, $s) or die(mysqli_error($link));
                    while($d = mysqli_fetch_assoc($r)){
                     $b_name = $d['batch_name'];
                     ?>
                     <input type="checkbox" <?php 
                        $sql = "select * from tbl_batchdocs where doc_id ='$did' and batch='".$b_name."'";  
                        $res = mysqli_query($link, $sql) or die(mysqli_error($link));  
                        if(mysqli_affected_rows($link) > 0)
                        {
                            echo 'checked';
                        }
                        
                        ?> class="form-check-inline" value="<?php echo $b_name; ?>" name="chklist[]"/><?php echo $b_name; ?> <br>
                     <?php   
                    }
                ?>
                </div>
             </div>
             <div class="row mt-2 mb-2">    
                <div class="col-12 col-md-4 offset-md-2">
                    <div class="input-group mt-1 mb-1">
                      <input type="hidden" name="docid" id="docid" value="<?php echo $did; ?>">
                      <button class="mt-1 btn btn-block" type="submit">Update</button>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="input-group mt-1 mb-1">
                      <a class="btn btn-block btn-danger" href="documents.php">Cancel</a>
                    </div>
                </div>
              </div>
             
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
  $(document).on('submit', '#editdoc-form', function()
    {  
            $.post('edit_doc.php', $(this).serialize(), function(data)
            {
                if(data=="s")
                {
                  window.location.href='documents.php';
                }
                else 
                {
                  $('#message').text(data);
                  $('#message').addClass('alert alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });

});

</script>
</body>
</html>