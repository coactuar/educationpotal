<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["superadmin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["superadmin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Documents</title>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="../css/styles.css">

</head>

<body class="admin">
<nav class="navbar sticky-top navbar-expand-lg  bg-dark">
  <a class="navbar-brand" href="#"><img src="../img/logo.png" class="img-fluid logo" alt=""/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="audiovideo.php">Audio/Videos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="documents.php">Documents</a>
      </li>
      
      
      
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?action=logout">Logout</a>
      </li>
      
    </ul>
  </div>
</nav>
<div class="container-fluid">
    <div class="row mt-1 p-0">
        <div class="col-12">
            <a href="adddoc.php" class="btn btn-group-sm btn-warning">Add Document</a>
        </div>
    </div>
    <div class="row mt-0 p-3">
        <div class="col-12">
            <div id="docs"> </div>
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
$(function(){
getDocs('1');
});

function update(pageNum)
{
  getDocs(pageNum);
}

function getDocs(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getdocs', page: pageNum},
        type: 'post',
        success: function(response) {
            
            $("#docs").html(response);
            
        }
    });
    
}

function deldoc(did)
{
    if(confirm('Are you sure?'))
    {
        $.ajax({
            url: 'ajax.php',
            data: {action: 'deldoc', docid: did},
            type: 'post',
            success: function(response) {
                
                getDocs('1');
                
            }
        });
    }
}


</script>

</body>
</html>