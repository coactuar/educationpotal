<?php
require_once "config.php";
/*require_once "sesvalidate.php";
echo $isLoggedIn;
if ($isLoggedIn) {
    header("location: dashboard.php");
    exit;
}
*/
$batch = -1;
$code = -1;
$num = '';
if(isset($_GET['batch_number']))
{
    $batch = 'Batch '.$_GET['batch_number'];
}
if(isset($_GET['country_code']))
{
    $code = $_GET['country_code'];
}
if(isset($_GET['mobile_number']))
{
    $num = $_GET['mobile_number'];
}


//echo $batch;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Session Videos :: Freedom From Diabetes</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="assets/fontawesome/css/all.min.css">
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-6 d-block d-md-none p-0 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
        <div class="col-12 col-md-3">
        <img src="img/logo.png" class="img-fluid img-logo mt-2" alt=""/> 
        </div>
        <div class="col-12 col-md-6 d-none d-md-block p-0 text-center">
        <img src="img/title.png" class="img-fluid img-title" alt=""/> 
        </div>
    </div>
</div>
<div class="container">    
    <div class="row">
        
        <div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 text-center">
           <div class="login-area">
            <form id="login-form" method="post">
              <h1>Login</h1>
              <div id="login-message" <?php
              if(isset($_SESSION['message']))
              {
                  echo " class='alert-danger'";
              }
              ?>
              >
              <?php
              if(isset($_SESSION['message']))
              {
                  echo $_SESSION['message'];
                  unset($_SESSION["message"]);
              }
              ?>
              </div>
              <div class="input-group mt-1 mb-1">
                <select id="batch" name="batch" class="form-control" required>
                    <option value="-1" <?php if ($batch == '-1') echo 'selected'; ?>>Select Batch</option>
                    <?php
                    $query="SELECT * FROM tbl_batches where active ='1' order by batch_name asc";
                    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
                    while($data = mysqli_fetch_assoc($res))
                    {
                        
                        //$batch_id = ($data['batch_id']);
                     ?>
                     <option value="<?php echo $data['batch_id']; ?>" <?php if ($batch == $data['batch_id']) echo 'selected'; ?>><?php echo $data['batch_name']; ?></option>
                     <?php
                    }
                    ?>
                </select>    
              </div>
              <div class="input-group mt-1 mb-1">
                <select id="country" name="country" class="form-control" required>
                    <option value="-1">Select Country Code</option>
                    <?php
                    $query="SELECT * FROM tbl_countries order by id";
                    $res = mysqli_query($link, $query) or die(mysqli_error($link)); 
                    while($data = mysqli_fetch_assoc($res))
                    {
                        
                        $country = ($data['country']);
                        if($country !='')
                        {
                     ?>
                     <option value="<?php echo $data['cntry_code']; ?>" <?php if ($code == $data['cntry_code']) echo 'selected'; ?>><?php echo $country.'(+'.$data['cntry_code'].')'; ?></option>
                     <?php
                        }
                    }
                    ?>
                </select>
              </div>
              <div class="input-group mt-1 mb-1">
                <input type="number" class="form-control" placeholder="Phone Number" aria-label="Phone Number" value="<?php echo $num; ?>" aria-describedby="basic-addon1" name="phnNum" id="phnNum" required>
              </div>
              
              <div class="input-group mt-1 mb-1">
                <button class="mt-4 btn btn-block" type="submit">Login <span class="fa fa-chevron-right fa-fw mr-3"></span></button>
              </div>
            </form>
           </div> 
        </div>
    </div>
</div>
<nav class="navbar fixed-bottom bottom-nav">
  <div class="icons">
            <a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><i class="fas fa-2x fa-globe web"></i></a><a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-facebook-square fb"></i></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><i class="fab fa-2x fa-youtube yt"></i></a><a href="https://www.instagram.com/freedomfromdiabetes/" target="_blank"><i class="fab fa-2x fa-instagram insta"></i></a>
            </div>
</nav>

<script src="js/jquery.min.js"></script>
<script>
$(function(){

  $('.input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $(".input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#batch').val() == '-1')
    {
        alert('Please select your batch');
        $('#batch').focus();
        return false;
    }
    
    if($('#country').val() == '-1')
    {
        alert('Please select country code');
        $('#country').focus();
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      console.log(data);
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other location and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your phone numer is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="-2")
      {
        $('#login-message').text('Your phone numer is not yet approved. Please wait for FFD Team for approval.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'dashboard.php';   
      }
      else
      {
        $('#login-message').text(data);
        $('#login-message').addClass('alert-danger');
      }
      
  });
  
  return false;
});

});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-17"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-17');
</script>

</body>
</html>